# -*- coding: utf-8 -*-
"""GUI-Application to write bootable disk images to USB keys"""


APP_NAME = "usbimager"
APP_TITLE = "USB-Imager"

__version__ = "3.0.0"

APP_LICENSE = "GPLv3+"
APP_COPYRIGHT = "© 2021-2025"

APP_ORGANISATION = "Secu-Design®"
APP_ID = "com.secu-design.app.usbimager"
APP_AUTHOR = "Skynet-Devel"
APP_AUTHOR_EMAIL = "skynet.devel@secu-design.com"

APP_URL_GITLAB = "https://gitlab.com/secu-design/usb-imager"
APP_URL_PYPI = "https://pypi.org/project/usb-imager"

APP_ABOUT = f"""\
Copyright {APP_COPYRIGHT} by {APP_ORGANISATION}.
Released under the {APP_LICENSE} license.

Author:  {APP_AUTHOR}
E-mail:  {APP_AUTHOR_EMAIL}

Websites:
{APP_URL_GITLAB}
{APP_URL_PYPI}

Please send me feedback if you like
USB-Imager or if you find bugs.
"""
